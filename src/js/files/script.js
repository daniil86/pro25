import { addMoney, getRandom, deleteMoney } from '../files/functions.js';
import { startData } from './startData.js';


export function initStartData() {
	drawStartCurrentItem();

	if (sessionStorage.getItem('money')) {
		writeScore();
	} else {
		sessionStorage.setItem('money', startData.bank);
		writeScore();
	}

	drawStartOpenedLevels();

	if (!sessionStorage.getItem('health')) sessionStorage.setItem('health', 0);

	initSatrtCurrentLevel();
}

function writeScore() {
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
}

initStartData();

//========================================================================================================================================================

if (document.querySelector('.main')) {
	document.querySelector('.main').classList.add('_active');

	drawPrices();
	checkBoughtItems();
	removeSelectedItems();
	writeSelected();

	drawStartOpenedLevels();
	openEneblesLevels();
}

function drawStartCurrentItem() {
	if (!sessionStorage.getItem('current-item')) sessionStorage.setItem('current-item', 1);
	if (!sessionStorage.getItem('item-1')) sessionStorage.setItem('item-1', true);
}

function drawPrices() {
	document.querySelector('[data-price="1"]').textContent = startData.prices.price_1;
	document.querySelector('[data-price="2"]').textContent = startData.prices.price_2;
	document.querySelector('[data-price="3"]').textContent = startData.prices.price_3;
	document.querySelector('[data-price="4"]').textContent = startData.prices.price_4;
}

export function checkBoughtItems() {
	if (sessionStorage.getItem('item-1')) {
		document.querySelector('[data-shop-button="1"]').textContent = '';
		document.querySelector('[data-item="1"]').classList.add('_bought');
	}
	if (sessionStorage.getItem('item-2')) {
		document.querySelector('[data-shop-button="2"]').textContent = '';
		document.querySelector('[data-item="2"]').classList.add('_bought');
	}
	if (sessionStorage.getItem('item-3')) {
		document.querySelector('[data-shop-button="3"]').textContent = '';
		document.querySelector('[data-item="3"]').classList.add('_bought');
	}
}

function removeSelectedItems() {
	const blocks = document.querySelectorAll('.shop__item');

	blocks.forEach(block => {
		if (block.classList.contains('_selected')) block.classList.remove('_selected');
	})
}

export function writeSelected() {
	if (document.querySelector('.player-ship__body').classList.contains('_ship-2')) {
		document.querySelector('.player-ship__body').classList.remove('_ship-2');
	}

	if (+sessionStorage.getItem('current-item') === 1) {
		document.querySelector('[data-shop-button="1"]').textContent = '';
		document.querySelector('[data-item="1"]').classList.add('_selected');
		document.querySelector('.player-ship__ship').style.backgroundImage = 'url("img/ships/ship-1.png")';
	} else if (+sessionStorage.getItem('current-item') === 2) {
		document.querySelector('[data-shop-button="2"]').textContent = '';
		document.querySelector('[data-item="2"]').classList.add('_selected');
		document.querySelector('.player-ship__ship').style.backgroundImage = 'url("img/ships/ship-2.png")';
		document.querySelector('.player-ship__body').classList.add('_ship-2');
	} else if (+sessionStorage.getItem('current-item') === 3) {
		document.querySelector('[data-shop-button="3"]').textContent = '';
		document.querySelector('[data-item="3"]').classList.add('_selected');
		document.querySelector('.player-ship__ship').style.backgroundImage = 'url("img/ships/ship-3.png")';
	}
}


// Открываем стартовые уровни
function drawStartOpenedLevels() {
	// if (!sessionStorage.getItem('level-1')) sessionStorage.setItem('level-1', true);
	if (!sessionStorage.getItem('opened-levels')) sessionStorage.setItem('opened-levels', 1)
}

function openEneblesLevels() {
	const levels = document.querySelectorAll('[data-level]');
	const openedLevels = +sessionStorage.getItem('opened-levels');

	for (let i = 0; i < openedLevels; i++) {
		levels[i].classList.remove('_closed');
	}
}

function initSatrtCurrentLevel() {
	if (!sessionStorage.getItem('current-level')) sessionStorage.setItem('current-level', 1);
}

export function writeCurrentHealthCount() {
	document.querySelector('.game__bottle-circle span').textContent = sessionStorage.getItem('health');
}

export function drawCurrentBackgorund(num) {
	const gameBody = document.querySelector('.game__body');
	gameBody.style.backgroundImage = `url("img/backgrounds/level-${num}.png")`;
}

//========================================================================================================================================================
// game

class Bullet {
	constructor(x, item, idx, parent) {
		this.x = x;
		this.y = -20;
		this.item = item;
		this.parent = parent;

		this.width = 35;
		this.height = 35;

		this.idx = idx;

		this.inGame = false;

		this.vy = getRandom(-5, -13);
		this.vx = 5;
		this.xOffset = 0.05;
		this.gravity = 0.3;

		this.draw();
	}

	draw() {
		this.bullet = document.createElement('div');
		this.bullet.classList.add('bullet');

		this.bullet.setAttribute('data-idx', this.idx);

		this.bullet.style.top = `${this.y}px`;
		this.bullet.style.left = `${this.x}px`;

		this.item.append(this.bullet);
	}

	createDot(x, y) {
		let dot = document.createElement('div');
		dot.classList.add('_dot');
		document.querySelector('.game').append(dot);
		dot.style.top = `${y}px`;
		dot.style.left = `${x}px`;
	}

	update() {

		if (this.inGame) {
			// Записываем текущую координату мяча
			const yBall = this.bullet.getBoundingClientRect().top;
			const xBall = this.bullet.getBoundingClientRect().left;


			// Если просто вниз летим
			let item1 = document.elementFromPoint(xBall + this.width, yBall + this.height + 3);

			// Если пуля при запуске вылетает вверх - разрешаем движение
			if (!item1) {
				this.vy += this.gravity;
				this.y += this.vy;

				this.vx += this.xOffset;
				this.x += this.vx;

				this.bullet.style.transform = `translate(${this.x}px, ${this.y}px)`;
			} else {
				// При выпуске пули - не реагируем на свое поле
				if (!item1.closest('.target') || (item1.closest('.target') && item1.closest(this.parent))) {
					this.vy += this.gravity;
					this.y += this.vy;

					this.vx += this.xOffset;
					this.x += this.vx;

					this.bullet.style.transform = `translate(${this.x}px, ${this.y}px)`;

				}
				// Если попали в поле противника
				else if (item1.closest('.target') && !item1.closest(this.parent)) {

					this.createFire(xBall, yBall);

					this.reset();

					//====
					// Снимаем урон если пуля попадает в корабль противника
					if (this.parent == '.enemy-ship') {
						// снимаем урон у игрока
						config.currentPlayerHealth -= config.enemyDamage;
						if (config.currentPlayerHealth < 0) config.currentPlayerHealth = 0;
						drawCurrentHealth(
							'[data-health="player"] .health-box__indicator',
							'[data-health="player"] .health-box__body span',
							config.currentPlayerHealth
						)
					} else if (this.parent == '.player-ship') {
						// снимаем урон у врага
						config.currentEnemyHealth -= config.playerDamage;
						if (config.currentEnemyHealth < 0) config.currentEnemyHealth = 0;

						drawCurrentHealth(
							'[data-health="enemy"] .health-box__indicator',
							'[data-health="enemy"] .health-box__body span',
							config.currentEnemyHealth
						)
					}

				}
			}

			if (this.y > innerHeight + this.height) {
				this.reset();
			}
		}

	}

	createFire(xBall, yBall, turn) {
		const xNumber = getRandom(10, 100);
		const yNumber = getRandom(10, 100);

		const fire = document.createElement('div');
		fire.classList.add('fire');

		if (this.parent == '.enemy-ship') {
			fire.style.left = `${xBall - xNumber}px`;
		} else if (this.parent == '.player-ship') {
			fire.style.left = `${xBall + xNumber}px`;
		}
		fire.style.top = `${yBall + yNumber}px`;

		document.querySelector('.game').append(fire);
	}

	reset() {
		this.inGame = false;

		this.x = 0;
		this.y = 0;

		this.vy = getRandom(-5, -13);
		this.vx = 5;

		this.bullet.style.transform = `translate(${this.x}px, ${this.y}px)`;

		this.bullet.classList.remove('_active');

	}
}

export const config = {
	state: 1, // 1 - не играем, 2 - игра началась

	enemyBullets: [],
	playerBullets: [],
	idx: 0,

	maxPlayerBullets: 10,
	maxEnemyBullets: 10,
	timeCreateBulletEnemy: 0,
	timelimitCreateBulletEnemy: 500,

	//==
	constEnemyHealth: 200,
	constPlayerHealth: 200,

	currentEnemyHealth: 200,
	currentPlayerHealth: 200,

	playerDamage: 20,
	enemyDamage: 20,

	//== 
	// Money
	winMoney: 3000,
	minusMoney: 5000,

	//==
	// Health
	healthUp: 20,

	//==

	playerCannon: document.querySelector('.player-ship__cannon'),
	enemyCannon: document.querySelector('.enemy-ship__cannon'),
	prompt: document.querySelector('.game__prompt'),
	promptText: document.querySelector('.game__prompt span'),
}

const promptTextConfig = {
	start: 'Tap the screen to shoot. 1 tap- 1 shot',
	health: 'Drink RUM to increase HP'
}


if (document.querySelector('.game')) {
	createBullets();
}

function drawCurrentHealth(indicator, text, count) {
	document.querySelector(indicator).style.width = `${count / 2}%`;
	document.querySelector(text).textContent = count;
}

export function startGame() {
	config.state = 2; // флаг, что игру начали
	setStartEnemyDamage();
	setPlayerDamage();

	animateGame(0);
}

function setStartEnemyDamage() {
	const currentLevel = +sessionStorage.getItem('current-level');
	if (currentLevel === 1) config.enemyDamage = 20;
	else if (currentLevel === 2) config.enemyDamage = 30;
	else if (currentLevel === 3) config.enemyDamage = 40;
	else if (currentLevel === 4) config.enemyDamage = 50;
	else if (currentLevel === 5) config.enemyDamage = 60;
}

function setPlayerDamage() {
	const currentItem = +sessionStorage.getItem('current-item');
	if (currentItem === 1) config.playerDamage = 20;
	else if (currentItem === 2) config.playerDamage = 30;
	else if (currentItem === 3) config.playerDamage = 40;
}

export function createGunPlayer() {
	const bullets = document.querySelectorAll('.bullet');

	for (let i = 0; i < config.playerBullets.length; i++) {
		if (config.playerBullets[i].inGame) continue;
		else if (!config.playerBullets[i].inGame) {
			config.playerBullets[i].inGame = true;
			bullets.forEach(bullet => {
				if (bullet.getAttribute('data-idx') == config.playerBullets[i].idx) {
					bullet.classList.add('_active');
				}
			})
		}
		break;
	}
}

export function createGunEnemy(deltaTime) {

	if (config.timeCreateBulletEnemy > config.timelimitCreateBulletEnemy) {
		const bullets = document.querySelectorAll('.bullet');

		for (let i = 0; i < config.enemyBullets.length; i++) {
			if (config.enemyBullets[i].inGame) continue;
			else if (!config.enemyBullets[i].inGame) {
				config.enemyBullets[i].inGame = true;
				bullets.forEach(bullet => {
					if (bullet.getAttribute('data-idx') == config.enemyBullets[i].idx) {
						bullet.classList.add('_active');
					}
				})
			}
			break;
		}
		config.timeCreateBulletEnemy = 0;
	} else {
		config.timeCreateBulletEnemy += deltaTime;
	}

}

let lastTime = 0;

function animateGame(timeStamp) {

	const deltaTime = timeStamp - lastTime;
	lastTime = timeStamp;

	createGunEnemy(deltaTime);

	checkGameOver();
	showPromptWhenLowHealth();


	config.playerBullets.forEach(bullet => {
		if (bullet.inGame) {
			bullet.update();
		}
	});

	config.enemyBullets.forEach(bullet => {
		if (bullet.inGame) {
			bullet.update();
		}
	});

	//==================


	if (config.state === 2) requestAnimationFrame(animateGame);

}

function createBullets() {
	for (let i = 0; i < config.maxPlayerBullets; i++) {
		config.playerBullets.push(new Bullet(20, config.playerCannon, config.idx, '.player-ship'));
		config.idx++;
	}
	for (let i = 0; i < config.maxEnemyBullets; i++) {
		config.enemyBullets.push(new Bullet(20, config.enemyCannon, config.idx, '.enemy-ship'));
		config.idx++;
	}
}

function checkGameOver() {
	if (config.currentPlayerHealth <= 0) {
		config.state = 3;
		showFinalScreen();
		deleteMoney(config.minusMoney, '.score', 'money');

		setTimeout(() => {
			hideBullets();
		}, 1000);
	}
	if (config.currentEnemyHealth <= 0) {
		config.state = 3;

		showFinalScreen(5000, 'win');

		addMoney(config.winMoney, '.score', 1000, 2000);

		levelUp();
		openEneblesLevels();

		setTimeout(() => {
			hideBullets();
		}, 1000);
	}
}

function hideBullets() {
	config.enemyBullets.forEach(bullet => bullet.reset());
	config.playerBullets.forEach(bullet => bullet.reset());
}

export function resetData() {
	// Обнуляем здоровье
	config.currentEnemyHealth = config.constEnemyHealth;
	config.currentPlayerHealth = config.constPlayerHealth;

	drawCurrentHealth(
		'[data-health="player"] .health-box__indicator',
		'[data-health="player"] .health-box__body span',
		config.currentPlayerHealth
	)
	drawCurrentHealth(
		'[data-health="enemy"] .health-box__indicator',
		'[data-health="enemy"] .health-box__body span',
		config.currentEnemyHealth
	)

	// Убираем огоньки
	document.querySelectorAll('.fire').forEach(fire => fire.remove());

	config.state = 1;

	resetAndShowPrompt();
}

function levelUp() {
	const openedLevels = +sessionStorage.getItem('opened-levels');
	const currentLevel = +sessionStorage.getItem('current-level');
	if (openedLevels < 5) {
		if (openedLevels === currentLevel) {
			sessionStorage.setItem('opened-levels', openedLevels + 1);
		}
	}
}

export function useHealthUP() {
	const health = +sessionStorage.getItem('health');

	if (health > 0) {
		sessionStorage.setItem('health', health - 1);
		writeCurrentHealthCount();
		config.currentPlayerHealth += config.healthUp;

		drawCurrentHealth(
			'[data-health="player"] .health-box__indicator',
			'[data-health="player"] .health-box__body span',
			config.currentPlayerHealth
		)
	}
}

function showPromptWhenLowHealth() {
	const prompt = document.querySelector('.game__prompt');
	const promptText = document.querySelector('.game__prompt span');

	if (config.currentPlayerHealth <= 100) {
		promptText.textContent = promptTextConfig.health;
		setTimeout(() => {
			prompt.classList.remove('_hide');
		}, 250);
	}
}

function resetAndShowPrompt() {
	const prompt = document.querySelector('.game__prompt');
	const promptText = document.querySelector('.game__prompt span');

	promptText.textContent = promptTextConfig.start;
	setTimeout(() => {
		prompt.classList.remove('_hide');
	}, 250);
}

export function showFinalScreen(count = 0, status = 'lose') {
	const final = document.querySelector('.final');
	const finalCheck = document.querySelector('.final-check');
	const finalTitle = document.querySelector('.final__title');

	if (final.classList.contains('_lose')) final.classList.remove('_lose');

	if (status === 'win') {
		finalTitle.textContent = 'Win!';
		finalCheck.textContent = count;
	} else {
		finalTitle.textContent = 'DEFEAT';
		finalCheck.textContent = `-5000`;

		final.classList.add('_lose');
	}

	setTimeout(() => {
		final.classList.add('_visible');
	}, 500);
}



