
import { deleteMoney, noMoney, checkRemoveAddClass } from './functions.js';
import { startData } from './startData.js';
import {
	checkBoughtItems, writeSelected, initStartData, createGunPlayer, config,
	startGame, writeCurrentHealthCount, drawCurrentBackgorund, resetData, useHealthUP
} from './script.js';


// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {
	initStartData();

	const targetElement = e.target;

	const wrapper = document.querySelector('.wrapper');

	const money = +sessionStorage.getItem('money');

	if (targetElement.closest('.preloader__button')) {
		location.href = 'main.html';
	}

	if (targetElement.closest('[data-btn="game-home"]')) {
		wrapper.classList.remove('_game');
		resetData();
	}

	if (targetElement.closest('[data-level="1"]') && !targetElement.closest('[data-level="1"]').classList.contains('_closed')) {
		sessionStorage.setItem('current-level', 1);
		writeCurrentHealthCount();
		drawCurrentBackgorund(1);

		setTimeout(() => {
			wrapper.classList.add('_game');
			startGame();
		}, 250);
	}

	if (targetElement.closest('[data-level="2"]') && !targetElement.closest('[data-level="2"]').classList.contains('_closed')) {
		sessionStorage.setItem('current-level', 2);
		writeCurrentHealthCount();
		drawCurrentBackgorund(2);
		setTimeout(() => {
			wrapper.classList.add('_game');
			startGame();
		}, 250);
	}

	if (targetElement.closest('[data-level="3"]') && !targetElement.closest('[data-level="3"]').classList.contains('_closed')) {
		sessionStorage.setItem('current-level', 3);
		writeCurrentHealthCount();
		drawCurrentBackgorund(3);
		setTimeout(() => {
			wrapper.classList.add('_game');
			startGame();
		}, 250);
	}

	if (targetElement.closest('[data-level="4"]') && !targetElement.closest('[data-level="4"]').classList.contains('_closed')) {
		sessionStorage.setItem('current-level', 4);
		writeCurrentHealthCount();
		drawCurrentBackgorund(4);
		setTimeout(() => {
			wrapper.classList.add('_game');
			startGame();
		}, 250);
	}

	if (targetElement.closest('[data-level="5"]') && !targetElement.closest('[data-level="5"]').classList.contains('_closed')) {
		sessionStorage.setItem('current-level', 5);
		writeCurrentHealthCount();
		drawCurrentBackgorund(5);
		setTimeout(() => {
			wrapper.classList.add('_game');
			startGame();
		}, 250);
	}



	//========================================================================================================================================================
	//shop

	if (targetElement.closest('[data-item="1"]') && !targetElement.closest('[data-item="1"]').classList.contains('_bought')) {
		if (money >= startData.prices.price_1) {
			deleteMoney(startData.prices.price_1, '.score');
			sessionStorage.setItem('item-1', true);
			checkBoughtItems();
		} else noMoney('.score');
	} else if (targetElement.closest('[data-item="1"]') && targetElement.closest('[data-item="1"]').classList.contains('_bought')) {
		checkRemoveAddClass('.shop__item', '_selected', document.querySelector('[data-item="1"]'));
		sessionStorage.setItem('current-item', 1);
		writeSelected();
	}

	if (targetElement.closest('[data-item="2"]') && !targetElement.closest('[data-item="2"]').classList.contains('_bought')) {
		if (money >= startData.prices.price_2) {
			deleteMoney(startData.prices.price_2, '.score');
			sessionStorage.setItem('item-2', true);
			checkBoughtItems();
		} else noMoney('.score');
	} else if (targetElement.closest('[data-item="2"]') && targetElement.closest('[data-item="2"]').classList.contains('_bought')) {
		checkRemoveAddClass('.shop__item', '_selected', document.querySelector('[data-item="2"]'));
		sessionStorage.setItem('current-item', 2);
		writeSelected();
	}

	if (targetElement.closest('[data-item="3"]') && !targetElement.closest('[data-item="3"]').classList.contains('_bought')) {
		if (money >= startData.prices.price_3) {
			deleteMoney(startData.prices.price_3, '.score');
			sessionStorage.setItem('item-3', true);
			checkBoughtItems();
		} else noMoney('.score');
	} else if (targetElement.closest('[data-item="3"]') && targetElement.closest('[data-item="3"]').classList.contains('_bought')) {
		checkRemoveAddClass('.shop__item', '_selected', document.querySelector('[data-item="3"]'));
		sessionStorage.setItem('current-item', 3);
		writeSelected();
	}

	if (targetElement.closest('[data-item="4"]') && money >= startData.prices.price_4) {
		deleteMoney(startData.prices.price_4, '.score', 'money');
		const health = +sessionStorage.getItem('health');
		sessionStorage.setItem('health', health + 1);
		writeCurrentHealthCount();
	} else if (targetElement.closest('[data-item="4"]') && money < startData.prices.price_4) {
		noMoney('.score');
	}

	//========================================================================================================================================================
	// game
	if (targetElement.closest('.game') && !targetElement.closest('.game__bottle') && !targetElement.closest('.game__button-home') && config.state === 2) {
		createGunPlayer();
		if (!config.prompt.classList.contains('_hide')) config.prompt.classList.add('_hide');
	}

	//===
	if (targetElement.closest('.final__button') && document.querySelector('.final').classList.contains('_visible')) {
		document.querySelector('.final').classList.remove('_visible');
		wrapper.classList.remove('_game');
		resetData();
	}

	if (targetElement.closest('.game__bottle')) {
		useHealthUP();
	}

})



