const startData = {
	bank: 50000, // 1000

	countBet: 50,
	maxBet: 950,

	prices: {
		price_1: 50,
		price_2: 12000,
		price_3: 25000,
		price_4: 1000,
	}

}

export { startData }